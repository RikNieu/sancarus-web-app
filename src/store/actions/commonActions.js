// Example
export const EXAMPLE = "EXAMPLE";
export const addTodo = text => {
  return {
    type: EXAMPLE,
    text
  };
};

// Nothing
export const NOTHING = "NOTHING";
export const nothing = () => {
  return { type: NOTHING };
};
