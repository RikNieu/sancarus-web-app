import React from "react";
import * as Actions from "../../actions";
import { ActionsObservable } from "redux-observable";

// Epcis to test
import { exampleEpic } from "../commonEpics";

// Import dependencies to mock
import store from "../../store";
// import callApi from "../../ApiUtils";

// Mocking dependencies
jest.mock("../../ApiUtils");
jest.mock("../../store");
jest.mock("../../../configs/variables");
function FormDataMock() {
  this.append = jest.fn();
}
global.FormData = FormDataMock;

// Test runs
describe("commonEpics tests", () => {
  beforeEach(() => {});

  it("Testing exampleEpic", () => {
    const actions$ = ActionsObservable.of({
      type: Actions.EXAMPLE
    });
    const apiReplyModel = 1;
    callApi.mockResolvedValue(apiReplyModel);
    return exampleEpic(actions$)
      .toArray()
      .toPromise()
      .then(actionReceived => {
        expect(actionReceived).toEqual([{ type: Actions.NOTHING }]);
      });
  });
});
