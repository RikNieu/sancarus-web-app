import React from "react";

import { combineEpics, ofType } from "redux-observable";
import {
  mergeMap,
  catchError,
  concatMap,
  flatMap,
  map,
  mapTo
} from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { of, throwError, concat, merge } from "rxjs";

import * as Actions from "../actions/commonActions";
// import callApi from "../ApiUtils";
// import { server } from "../../configs/variables";
import store from "../store";

// export const exampleEpic = (action$, state) =>
//   // action$.ofType(Actions.EXAMPLE).map(() => ({ type: Actions.NOTHING }));
//   action$.pipe(
//     ofType(Actions.EXAMPLE),
//     mapTo({ type: Actions.NOTHING })
//   );

export const exampleEpic = action$ =>
  action$.pipe(
    ofType(Actions.EXAMPLE),
    mapTo({ type: Actions.NOTHING })
  );

const commonEpics = combineEpics(exampleEpic);

export default commonEpics;
