import { combineEpics } from "redux-observable";
import "rxjs";

import exampleEpic from "./commonEpics";

const rootEpic = combineEpics(exampleEpic);

export default rootEpic;
