import commonStore, { initialState } from "../commonReducer";

describe("modalStore reducers", () => {
  beforeEach(() => {});

  test("Setting ANIMATION_MODAL_ON", () => {
    const text = "textTest";
    const action = {
      type: "EXAMPLE",
      text
    };
    const newState = commonStore(initialState, action);
    expect(newState).toEqual({
      ...initialState
    });
  });
});
