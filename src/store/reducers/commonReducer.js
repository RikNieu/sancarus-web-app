import { EXAMPLE } from "../actions/commonActions";

export const initialState = {
  example: []
};

export default function commonStore(state = initialState, action) {
  switch (action.type) {
    case EXAMPLE: {
      return Object.assign({}, state, { example: [] });
    }
    default:
      return state;
  }
}
