import { combineReducers } from "redux";

import commonStore from "./commonReducer";

const CombinedStates = combineReducers({
  commonStore
});

export default CombinedStates;
